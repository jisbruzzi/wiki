# Subir imagen
```
gcloud builds submit . --tag gcr.io/jisbruzzi-zonia-music/wiki --project=jisbruzzi-zonia-music
```

# Entrar a la imagen
```
gcloud beta compute ssh --zone "us-central1-a" "instance-4" --project "jisbruzzi-zonia-music"
```

# Desplegar imagen subida
```
gcloud beta compute instances update-container "instance-4" --project "jisbruzzi-zonia-music"
```