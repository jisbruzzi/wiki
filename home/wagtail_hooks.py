from wagtail.core import hooks
from wagtail.admin.menu import MenuItem

@hooks.register('register_admin_menu_item')
def register_frank_menu_item():
  return MenuItem('Eventos', '/admin/pages/4/', classnames='icon icon-folder-inverse', order=10000)