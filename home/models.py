from django.db import models

from wagtail.core.models import Page
from modelcluster.contrib.taggit import ClusterTaggableManager
from taggit.models import TaggedItemBase

from modelcluster.fields import ParentalKey

from wagtail.contrib.routable_page.models import RoutablePageMixin, route
from wagtail.admin.edit_handlers import FieldPanel, InlinePanel, StreamFieldPanel
from wagtail.core.fields import StreamField
from wagtail.core.models import Page, Orderable
from wagtail.images.edit_handlers import ImageChooserPanel
from wagtail.search import index
from wagtail.snippets.edit_handlers import SnippetChooserPanel

from wagtail_blocks.blocks import HeaderBlock, ListBlock, ImageTextOverlayBlock, CroppedImagesWithTextBlock, \
    ListWithImagesBlock, ThumbnailGalleryBlock, ChartBlock, MapBlock, ImageSliderBlock
from wagtail.contrib.table_block.blocks import TableBlock
from wagtail.core import blocks

class HomePage(Page):
    pass

class FreeformPageTag(TaggedItemBase):
    """
    This model allows us to create a many-to-many relationship between
    the BlogPage object and tags. There's a longer guide on using it at
    http://docs.wagtail.io/en/latest/reference/pages/model_recipes.html#tagging
    """
    content_object = ParentalKey('FreeformPage', related_name='tagged_items', on_delete=models.CASCADE)

class FreeformPage(Page):
    """
    A Blog Page
    We access the People object with an inline panel that references the
    ParentalKey's related_name in BlogPeopleRelationship. More docs:
    http://docs.wagtail.io/en/latest/topics/pages.html#inline-models
    """
    body = StreamField([
            ('header', HeaderBlock()),
            ('list', ListBlock()),
            ('image_text_overlay', ImageTextOverlayBlock()),
            ('cropped_images_with_text', CroppedImagesWithTextBlock()),
            ('list_with_images', ListWithImagesBlock()),
            ('thumbnail_gallery', ThumbnailGalleryBlock()),
            ('chart', ChartBlock()),
            ('map', MapBlock()),
            ('image_slider', ImageSliderBlock()),
            ('table',TableBlock()),
            ('paragraph', blocks.RichTextBlock()),
        ],
        verbose_name="Page body", blank=True
    )
    tags = ClusterTaggableManager(through=FreeformPageTag, blank=True)

    content_panels = Page.content_panels + [
        StreamFieldPanel('body'),
        FieldPanel('tags'),
    ]

    search_fields = Page.search_fields + [
        index.SearchField('body'),
        index.SearchField('title'),
    ]